using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour{

    //Bounds to klasa z 2 Vector3, jedna okre�la pozycj� (center), a druga rozmiar(size).
    //Przydatne je�li chcemy tworzy� granice oparte o sze�cian
    [SerializeField] Bounds boundsAreaSpawn;

    [Space]

    [SerializeField] GameObject prefab;
    [SerializeField] float timeToSpawnNextObject = 2f;
    [SerializeField] int numberSpawnedObjectsAtOnce = 4;

    //Przechowuje akualny czas licznika
    float timer;

    //Message/Metoda z MonoBehaviour, kt�ra wykonuje si� w edytorze podczas modyfikowania kompnentu.
    //Przydatne je�eli chcemy walidowa� zmienne modyfikowalne z poziomu edytora.
    void OnValidate() {
        //Informujemy programist� �e w scenie w edytorze musi przypisa� prefab do komponentu
        if (prefab == null) {
            Debug.LogError("Ni ma prefabu :(");
        }
    }

    //Co klatk� dodawaj warto�� do timera i sprawd� czy zosta� on przekroczony. Je�li tak tw�rz obiekty na scenie.
    void Update() {
        timer += Time.deltaTime;
        if (timer > timeToSpawnNextObject) {
            for (int i = 0; i < numberSpawnedObjectsAtOnce; i++) {
                //Instantiate tworzy (a tak naprawd� kopiuje) obiekt na scenie za pomoc� innego obiektu, zazwyczaj jest to prefab
                Instantiate(prefab, GetRandomPosition(), Quaternion.identity);
            }
            timer = 0;
        }
    }

    //Generuje losow� pozycj� w obr�bie boundsAreaSpawn i samej pozycji Spawnera
    Vector3 GetRandomPosition() {
        Vector3 position = transform.position;
        position += boundsAreaSpawn.center;
        position += new Vector3(
            Random.Range(boundsAreaSpawn.min.x, boundsAreaSpawn.max.x),
            Random.Range(boundsAreaSpawn.min.y, boundsAreaSpawn.max.y),
            Random.Range(boundsAreaSpawn.min.z, boundsAreaSpawn.max.z)
            );
        return position;
    }

    //Metoda wywo�uj�ca si� w edytorze jak obiekt jest (lub jego rodzic) zaznaczony.
    //Gizmos to klasa do rysowania debugerskich rysunk�w na scenie. 
    //Tutaj u�yte aby zwizualizowa� granice spawnu
    void OnDrawGizmosSelected() {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(boundsAreaSpawn.center + transform.position, boundsAreaSpawn.size);
    }

}
